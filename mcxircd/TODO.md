TODO
====

Roadmap: 

* More complete implementation of RFC2812
* web-interface (minimal webserver, websockets, javascript IRC client)
* Server-server communications
   * MCXIRCD <-> MCXIRCD
   * MCXIRCD <-> ngircd (is this insanely complex?)
* true TOR/I2P-support

BUGS
====

* Method for matching users against wildcards when all they have is a nick - in channel banning (the only place where wildcards actually are used)
* user io.LimitedReader for reading from users

STUFF THAT SHOULD maybe BE DONE
===============================

* replace namespace message-passing with RWMutex, since that maybe is a bottleneck
* handshake (NICK, then USER, then create IRCUser-object)
* normal<-->anonymous channel binding (do this when channel MODEs are implemented!)
* handlers for all commands that are missing
  * OPER
  * MOTD (motd-file?)
  * KILL
  * ISON
* Server-to-server  
  * MCXIRCD-MCXIRCD
    * RSA-OAEP/PSS & AES-CTR
    * mesh network, not starfish!!!
    * special config-folder with one tar-file containing keys and config for each other server (easy distribution of configs for new nodes)
  * MCXIRCD-normal IRC servers
    * maybe not


commands that probably will not be implemented
----------------------------------------------

* CONNECT
* LINKS
* LUSERS
* TRACE
* SERVLIST
* INVITE


