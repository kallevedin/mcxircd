/*
 * handshake performs handshakes with connecting entities. In theory it figures 
 * out if the connecting entity is a user, a server, a service or something 
 * else, and creates such an object after the handshake is complete, but right 
 * now it can only handle users.
 * 
 * Essentially: "OK, someone connected! Lets see what it says. Oh... It looks 
 * like it thinks its an ordinary user! Then I will create a new IRCUser to 
 * handle this connection, and then I will destroy myself."
 */
package main

import (
	"net"
	"bufio"
	"strings"
	"time"
	"fmt"
	)

// Executes as a goroutine.
// Performs a handshake with whatever that connected. Currently, ONLY ordinary IRC clients are supported.
func handshake(conn net.Conn) {
	failure := true
	defer handshakeCleanup(conn, &failure)
	conn.SetReadDeadline(time.Now().Add(TIMEOUT_HANDSHAKE * time.Second))

	scanner := bufio.NewScanner(conn) // splits input at \r\n OR \n
	for {
		if !scanner.Scan() {
			err := scanner.Err()
			if err == nil { // connection closed
				return
			} else {
				switch t := err.(type) {
					case net.Error :
						if t.Timeout() {
							conn.Write([]byte("ERROR: Handshake timeout\r\n"))
							return
						}
					default :
						fmt.Printf("Handshake failure: %v", err)
						return
				}
			}
		}
		
		line := scanner.Text()
		wordScanner := bufio.NewScanner(strings.NewReader(line))
		wordScanner.Split(bufio.ScanWords)

		if !wordScanner.Scan() {
			continue
		}
		command := wordScanner.Text()
		switch command {
			// This is very accepting and forgiving!!!
			
			case "PASS" :
				continue

			case "USER" :
				continue

			case "NICK" :
				if !wordScanner.Scan() {
					conn.Write([]byte("ERROR: NO NICKNAME\r\n"))
					return
				}
				nickname := wordScanner.Text()
				_, ok := newIRCUser(conn, nickname)
				if !ok {
					conn.Write([]byte(ERR_NICKNAMEINUSE("anonymous", nickname)))
					continue
				}
				failure = false
				return
				
			default:
				conn.Write([]byte("ERROR: Handshake failure\r\n"))
				return
		}
	}
}


func handshakeCleanup(conn net.Conn, doCleanup *bool) {
	conn.SetReadDeadline(time.Time{})
	if *doCleanup {
		conn.Close()
	}
}
