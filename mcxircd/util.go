package main

import (
	"bufio"
	"fmt"
	"strings"
	)



type ScanWordsOrCUTObj struct {
    data        []byte
}
func makeScanWordsOrCUT() *ScanWordsOrCUTObj {
    return &ScanWordsOrCUTObj{data: make([]byte, 0)}
}
func (s *ScanWordsOrCUTObj) ScanWordsOrCUT(data []byte, atEOF bool) (advance int, token []byte, err error) {
    advance, token, err = bufio.ScanWords(data, atEOF)
    s.data = data
    return advance, token, err
}
func (s *ScanWordsOrCUTObj) CUT() []byte {
    return s.data
}




// returns the first word in a sentence (actually the first sequence of bytes that ends with a space)
func firstWord(sentence string) string {
    for i:=0; i!=len(sentence); i++ {
        if sentence[i] == ' ' { 
            return sentence[:i]
        }
    }
    return sentence
}

// same as firstWord, except that it also splits at ANYTHING that is not a letter or number
func firstSequenceOfLetters(sentence string) string {
	for i:=0; i!=len(sentence); i++ {
		if sentence[i] == ' ' || sentence[i] == '\x01' { // TODO REPLACE WITH !isletter() || !isnumber() etc
			return sentence[:i]
		}
	}
	return sentence
}


func RPL_WELCOME(nick string) string {
	return fmt.Sprintf(":%s 001 %s Welcome to the Internet Relay Network %s!%s@%s\r\n", server.FullName, nick, nick, nick, server.Hostcloak)
}

func RPL_YOURHOST(nick string) string {
	return fmt.Sprintf(":%s 002 %s Your host is %s, running version %s\r\n", server.FullName, nick, server.ShortName, server.Version)
}

func RPL_CREATED(nick string) string {
	return fmt.Sprintf(":%s 003 %s Om! Hare Krsna Hare Krsna - Krsna Krsna Hare Hare - Hare Rama Hare Rama - Rama Rama Hare Hare\r\n", server.FullName, nick)
}

func RPL_MYINFO(nick string) string {
	return fmt.Sprintf(":%s 004 %s %s %s aiowrs Aknot\r\n", server.FullName, nick, server.ShortName, server.Version)
}

func RPL_UMODEIS(nick, umode string) string {
	return fmt.Sprintf(":%s 221 %s %s\r\n", server.FullName, nick, umode)
}

func RPL_SIMPLEWHOISUSER(nick, target string) string {
	return RPL_WHOISUSER(nick, target, target, server.Hostcloak, server.DefaultRealName)
}

func RPL_WHOISUSER(nick, target, user, host, realName string) string {
	return fmt.Sprintf(":%s 311 %s %s %s %s * :%s\r\n", server.FullName, nick, target, user, host, realName)
}

func RPL_ENDOFWHO(nick, itemName string) string {
	return fmt.Sprintf(":%s 315 %s %s :End of WHO list\r\n", server.FullName, nick, itemName)
}

func RPL_ENDOFWHOIS(nick, target string) string {
	return fmt.Sprintf(":%s 318 %s %s :End of WHOIS list\r\n", server.FullName, nick, target)
}

func RPL_LIST(nick, channel, topic string, numVisible int) string {
	return fmt.Sprintf(":%s 322 %s %s %d :%s\r\n", server.FullName, nick, channel, numVisible, topic)
}

func RPL_LISTEND(nick string) string {
	return fmt.Sprintf(":%s 323 %s :End of LIST\r\n", server.FullName, nick)
}

func RPL_CHANNELMODEIS(nick, channel, modeString string) string {
	return fmt.Sprintf(":%s 324 %s %s %s\r\n", server.FullName, nick, channel, modeString)
}


func RPL_NOTOPIC(nick, channel string) string {
	return fmt.Sprintf(":%s 331 %s %s :No topic is set\r\n", server.FullName, nick, channel)
}

func RPL_TOPIC(nick, channel, topic string) string {
	return fmt.Sprintf(":%s 332 %s %s :%s\r\n", server.FullName, nick, channel, topic)
}

func RPL_MAYBETOPIC(nick, channel, topic string) string {
	if topic == "" { 
		return RPL_NOTOPIC(nick, channel) 
	}
	return RPL_TOPIC(nick, channel, topic)
}

func RPL_INVITELIST(nick, channel, inviteMask string) string {
	return fmt.Sprintf(":%s 346 %s %s %s\r\n", server.FullName, nick, channel, inviteMask)
}

func RPL_ENDOFINVITELIST(nick, channel string) string {
	return fmt.Sprintf(":%s 347 %s %s :End of channel invite list\r\n", server.FullName, nick, channel)
}

func RPL_EXCEPTLIST(nick, channel, exceptionMask string) string {
	return fmt.Sprintf(":%s 348 %s %s %s\r\n", server.FullName, nick, channel, exceptionMask)
}

func RPL_ENDOFEXCEPTLIST(nick, channel string) string {
	return fmt.Sprintf(":%s 349 %s %s\r\n", server.FullName, nick, channel)
}

func RPL_VERSION(nick string) string {
	return fmt.Sprintf(":%s 351 %s %s.0 %s :%s\r\n", server.FullName, nick, server.Version, server.ShortName, server.Quote)
}

func RPL_SIMPLEWHOREPLY(nick, channel, target string, hasOp bool) string {
	weirdsymbols := "H"
	if hasOp { weirdsymbols = "H@" }
	return RPL_WHOREPLY(nick, channel, target, server.Hostcloak, server.FullName, target, weirdsymbols, server.DefaultRealName, 0)
}

func RPL_WHOREPLY(nick, channel, user, host, usersServer, target, weirdsymbols, realName string, hopcount int) string {
	// It is unknown what the "wierdsymbols" mean. See RFC2812 page 46.
	return fmt.Sprintf(":%s 352 %s %s %s %s %s %s %s :%d %s\r\n", server.FullName, nick, channel, user, host, usersServer, target, weirdsymbols, hopcount, realName)
}
           
func RPL_NAMREPLY(nick, channel string, nicknames []string) string {
	typeOfChannel := "=" // normal chan
	if channel == "*" {
		typeOfChannel = "*"
	}
	result := fmt.Sprintf(":%s 353 %s %s %s :", server.FullName, nick, typeOfChannel, channel)
	for _, name := range nicknames {
		result += name + " "
	}
	strings.TrimSpace(result)
	result += "\r\n"
	return result
}

func RPL_ENDOFNAMES(nick, channel string) string {
	return fmt.Sprintf(":%s 366 %s %s :End of NAMES list\r\n", server.FullName, nick, channel)
}

func RPL_ENDOFBANLIST(nick, channel string) string {
	return fmt.Sprintf(":%s 368 %s %s :End of channel ban list\r\n", server.FullName, nick, channel)
}

func RPL_INFO(nick string) string {
	return fmt.Sprintf(":%s 371 %s :software: %s, version: %s\r\n", server.FullName, nick, server.Software, server.Version)
}

func RPL_MOTD(nick string) string {
	motdStr := ""
	if motd == nil {
		for i:=0; i!=10; i++ {
			motdStr += fmt.Sprintf(":%s 372 %s :- \x02\x034Om mani padme hum\x03\x02\r\n", server.FullName, nick)
		}
	} else {
		for i:=0; i!=len(motd); i++ {
			motdStr += fmt.Sprintf(":%s 372 %s :- %s\r\n", server.FullName, nick, string(motd[i]))
		}
	}
	return motdStr
}

func RPL_ENDOFINFO(nick string) string {
	return fmt.Sprintf(":%s 374 %s :End of INFO list\r\n", server.FullName, nick)
}

func RPL_MOTDSTART(nick string) string {
	return fmt.Sprintf(":%s 375 %s :- %s Message of the day - \r\n", server.FullName, nick, server.ShortName)
}

func RPL_ENDOFMOTD(nick string) string {
	return fmt.Sprintf(":%s 376 %s :End of MOTD command\r\n", server.FullName, nick)
}

func RPL_YOUREOPER(nick string) string {
	return fmt.Sprintf(":%s 381 %s :You are now an IRC operator\r\n", server.FullName, nick)
}

func PRIVMSG(from, to, message string) string {
	return fmt.Sprintf(":%s!%s@%s PRIVMSG %s :%s\r\n", from, from, server.Hostcloak, to, message)
}

func JOIN(nick, channel string) string {
	return fmt.Sprintf(":%s!%s@%s JOIN %s\r\n", nick, nick, server.Hostcloak, channel)
}

func KICK(nick, douchebag, channel, reason string) string {
	return fmt.Sprintf(":%s!%s@%s KICK %s %s :%s\r\n", nick, nick, server.Hostcloak, channel, douchebag, reason)
}

func NICK(oldNick, newNick string) string {
	return fmt.Sprintf(":%s!%s@%s NICK %s\r\n", oldNick, oldNick, server.Hostcloak, newNick)
}

func MODE(nick, target, modeString string) string {
	return fmt.Sprintf(":%s!%s@%s MODE %s %s\r\n", nick, nick, server.Hostcloak, target, modeString)
}

func PART(nick, channel string) string {
	return fmt.Sprintf(":%s!%s@%s PART %s :\r\n", nick, nick, server.Hostcloak, channel)
}

func ERR_NOSUCHNICK(nick, nonExistant string) string {
	return fmt.Sprintf(":%s 401 %s %s :No such nick/channel\r\n", server.FullName, nick, nonExistant)
}

func ERR_NOSUCHSERVER(nick, nonExistant string) string {
	return fmt.Sprintf(":%s 402 %s %s :No such server\r\n", server.FullName, nick, nonExistant)
}

func ERR_NOSUCHCHANNEL(nick, channel string) string {
	return fmt.Sprintf(":%s 403 %s %s :No such channel\r\n", server.FullName, nick, channel)
}

func ERR_WASNOSUCHNICK(nick, target string) string {
	return fmt.Sprintf(":%s 406 %s %s :There was no such nickname\r\n", server.FullName, nick, target)
}

func ERR_TOOMANYTARGETS(nick, someClue string) string {
	return fmt.Sprintf(":%s 407 %s %s :Too many targets.\r\n", server.FullName, nick, someClue)
}

func ERR_ERRONEUSNICKNAME(nick, badNick string) string {
	return fmt.Sprintf(":%s 432 %s %s :Erroneous nickname\r\n", server.FullName, nick, badNick)
}

func ERR_NICKNAMEINUSE(currentNick, desiredNick string) string {
	return fmt.Sprintf(":%s 433 %s %s :Nickname is already in use\r\n", server.FullName, currentNick, desiredNick)
}

func ERR_NOTONCHANNEL(nick, channel string) string {
	return fmt.Sprintf(":%s 442 %s %s :You're not on that channel\r\n", server.FullName, nick, channel)
}

func ERR_NICKCOLLISION(nick string) string {
	return fmt.Sprintf(":%s 436 %s :Nickname collision KILL from %s@%s", server.FullName, nick, nick, server.Hostcloak)
}

func ERR_USERNOTINCHANNEL(nick, notPresent, channel string) string {
	return fmt.Sprintf("%s 441 %s %s %s :They aren't on that channel\r\n", server.FullName, nick, notPresent, channel)
}

func ERR_NEEDMOREPARAMS(nick, command string) string {
	return fmt.Sprintf(":%s 461 %s %s :Not enough parameters\r\n", server.FullName, nick, command)
}

func ERR_PASSWDMISMATCH(nick string) string {
	return fmt.Sprintf(":%s 464 %s :Password incorrect\r\n", server.FullName, nick)
}

func ERR_UNKNOWNMODE(nick, channel string, badMode string) string {
	return fmt.Sprintf(":%s 472 %s %s :is unknown mode char to me for %s\r\n", server.FullName, nick, badMode, channel)
}

func ERR_BANNEDFROMCHAN(nick, channel string) string {
	return fmt.Sprintf(":%s 474 %s %s :Cannot join channel (+b)\r\n", server.FullName, nick, channel)
}

func ERR_BADCHANNELKEY(nick, channel string) string {
	return fmt.Sprintf(":%s 475 %s %s :Cannot join channel (+k)\r\n", server.FullName, nick, channel)
}

func ERR_NOPRIVILEGES(nick string) string {
	return fmt.Sprintf(":%s 481 %s :Permission Denied- You're not an IRC operator\r\n", server.FullName, nick)
}

func ERR_CHANOPRIVSNEEDED(nick, channel string) string {
	return fmt.Sprintf(":%s 482 %s %s :You're not channel operator\r\n", server.FullName, nick, channel)
}

func ERR_UMODEUNKNOWNFLAG(nick string) string {
	return fmt.Sprintf(":%s 501 %s :Unknown MODE flag\r\n", server.FullName, nick)
}

func ERR_USERSDONTMATCH(nick string) string {
	return fmt.Sprintf(":%s 502 %s :Cannot change mode for other users\r\n", server.FullName, nick)
}
