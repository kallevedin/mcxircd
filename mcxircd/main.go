/*
 * reads command line arguments
 * reads config file (todo!)
 * sets global variables
 * starts listeners on various interfaces (currently only one tcp)
 */
package main



import (
    "fmt"
    "flag"
    "net"
    "strconv"
    "runtime"
    )



const (
	TIMEOUT_HANDSHAKE = 60
	TIMEOUT_PING = 5*60
	TIMEOUT_PONG = 3*60
	)


var server Server
type Server struct {
	FullName	string
	ShortName	string
	Software	string
	Version		string
	Hostcloak	string
	Quote		string
	DefaultRealName	string
	MaxNickLen	int
}



func startListen(port int) {
    tcpAddr, e := net.ResolveTCPAddr("tcp", ":" + strconv.Itoa(port))
    if e != nil {
        fmt.Printf("Errous port number %d!\n", port)
        return
    }
    listener, e := net.ListenTCP("tcp", tcpAddr)
    if e != nil {
        fmt.Printf("Could not listen on port %d because -- %v\n", port, e)
        return
    }
    for {
        conn, err := listener.Accept()
        if err != nil {
            fmt.Printf("could not accept on port %d because %v", port, e)
            return
        }
        go handshake(conn)
    }
}



func main() {

    NCPU := runtime.NumCPU()
    runtime.GOMAXPROCS(NCPU + 1)

	server = Server{FullName: "mcxircd.org", ShortName: "mcxircd", Software: "mcxircd", Version: "mcxircd-0", Hostcloak: "cloak", Quote: "\x031,0 Om mani padme hum \x03", DefaultRealName: "Chelsea Manning", MaxNickLen: 9 }
	nameSpace = *makeNameSpace()

    // read flags
    port := flag.Int("port", 6667, "Port to listen at")
    conf := flag.String("conf", "./mcxircd.conf", "Config file to use.")
    motd := flag.String("motd", "./MOTD", "MOTD file to use.")
    opers := flag.String("opers", "./OPERS", "OPERS file")
    flag.Parse()

	// read config files
	readConfigFile(*conf)
	readMOTDFile(*motd)
	readOPERSFile(*opers)

    fmt.Printf("Now listening at port: %d\n", *port)

    startListen(*port)

}



