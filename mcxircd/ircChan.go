// Every IRC channel is an IRCChannel object, which consists of this structure 
// and also a goroutine (which spends most of its time in ircChanLoop, I guess).
package main



import (
	"strings"
	"bytes"
	"bufio"
	"fmt"
	"regexp"
	"sync"
	)



var (
	validChannelNameRegexp string = "^[#+].*$"
	validChannelNames *regexp.Regexp = regexp.MustCompile(validChannelNameRegexp)

	usersInChannels map[string]map[string]*IRCChannel = make(map[string]map[string]*IRCChannel)
	usersInChannelsMutex = new(sync.RWMutex)
	
	privmsgRegexpStr string = `^:` + validUsername + `!` + validUsername + `@.* PRIVMSG [#+].* :`
	privmsgRegexp *regexp.Regexp = regexp.MustCompile(privmsgRegexpStr)
	)



type IRCChanOp struct {
	User		*IRCUser
	Command		string
	ReplyTo		chan([]byte)
}



type IRCDistribute struct {
	From		string
	Message		[]byte
}



// Every IRC channel is an IRCChannel object, which consists of this structure and also
// a goroutine (which spends most of its time in ircChanLoop, I guess).
type IRCChannel struct {
	Name				string	
	Users				map[string]*IRCUser
	Operators			map[string]*IRCUser
	Banned				map[string]string
	Distribute			chan(*IRCDistribute)
	Operation			chan(*IRCChanOp)
	Topic				string
	
	Key					string
	
	// channel flags
	Anonymous			bool
	NoExternalMessages	bool
	TopicLock			bool
	LinkAnon			bool

}


// Creates a new IRC channel (See the IRCChannel struct)!
func createIRCChannel(name string) (*IRCChannel, bool) {
	if validChannelNames.Match([]byte(name)) {
		c := IRCChannel{Name: name, Users: make(map[string]*IRCUser), Operators: make(map[string]*IRCUser), Banned: make(map[string]string), Distribute: make(chan *IRCDistribute), Operation: make(chan *IRCChanOp), Topic: "", Key: ""}
		c.initChannelMode()
		registerNameOk := make(chan bool)
		nameSpace.Add <- &NSAddRequest{Key: c.Name, Value: &c, ReplyTo: registerNameOk}
		if registerOK := <-registerNameOk; registerOK {
			go c.ircChanLoop()
			return &c, true
		}
	}
	return nil, false
}

// returns the symbol representing the type of the channel (ie '#' or '+')
func (myself *IRCChannel) ChannelType() rune {
	return rune(myself.Name[0])
}

// Helper function that takes care of setting the correct mode flags for a 
// channel when it is created.
func (myself *IRCChannel) initChannelMode() {
	switch myself.ChannelType() {
		case '#' :
			myself.Anonymous = false
			myself.NoExternalMessages = true
			myself.TopicLock = false
			myself.LinkAnon = false
		case '+' :
			myself.Anonymous = true
			myself.NoExternalMessages = true
			myself.TopicLock = true
			myself.LinkAnon = false
	}
}

// Parses a mode change string, and if everything is alrighty, updates the 
// channels mode. Returns false is unsuccessful, and either the bad mode 
// character or the NUL character if there was parameter errors. See RFC2812 
// section 3.2.3 for more information.
func (myself *IRCChannel) ModifyMode(modString string) (string, bool) {
	LinkAnon := myself.LinkAnon
	NoExternalMessages := myself.NoExternalMessages
	TopicLock := myself.TopicLock
	Key := myself.Key
	mod := true

	words := bufio.NewScanner(strings.NewReader(modString))
	words.Split(bufio.ScanWords)
	for words.Scan() {
		s := bufio.NewScanner(strings.NewReader(words.Text()))
		s.Split(bufio.ScanRunes)
		for s.Scan() {
			r := s.Text()
			switch r {
				case "+" :
					mod = true
				case "-" :
					mod = false
				case "L" :
					LinkAnon = mod
				case "b" :
					if !words.Scan() { return "\x00", false }
					douchebag := words.Text()
					if mod {
						myself.Banned[douchebag]=douchebag
						// TODO: replace this with actual test against nick!user@hostcloak
						if _,present := myself.Users[douchebag]; present {
							for _,ircUser := range myself.Users {
								ircUser.rawOutput <- []byte(KICK("anonymous", douchebag, myself.Name, "excommunication"))
							}
							delete(myself.Users, douchebag)
							delete(myself.Operators, douchebag)
						}
					} else {
						delete(myself.Banned, douchebag)
					}
				case "k" :
					if mod {
						if !words.Scan() { return "\x00", false }
						Key = words.Text()
					} else {
						Key = ""
					}
				case "n" :
					NoExternalMessages = mod
				case "o" :
					if !words.Scan() { return "\x00", false }
					target := words.Text()
					user,isPresent := myself.Users[target]
					if !isPresent {
						// can not OP a nonpresent user
						// the IRC protocol does not cover this case, so treat it as a NOP
						continue
					}
					if mod {
						myself.Operators[target] = user
					} else {
						delete(myself.Operators, target)
					}
				case "t" :
					TopicLock = mod
				default :
					return r, false
			}
		}
		if err := s.Err(); err != nil {
		return "\x00", false
		}
	}
	if err := words.Err(); err != nil {
		return "\x00", false
	}
	myself.LinkAnon = LinkAnon
	myself.NoExternalMessages = NoExternalMessages
	myself.TopicLock = TopicLock
	myself.Key = Key
	return "\x00", true
}

func (myself *IRCChannel) ModeString() string {
	str := ""
	if myself.Anonymous { str += "A" }
	if myself.LinkAnon { str += "L" }
	if myself.Key != "" { str += "k" }
	if myself.NoExternalMessages { str += "n" }
	if myself.TopicLock { str += "t" }
	return str
}



// Listens for incomming messages that should be distributed to all connected users, and command messages.
func (myself *IRCChannel) ircChanLoop() {
	defer myself.CleanDestroy()
	for {
		select {
			case message := <-myself.Distribute :
				if myself.NoExternalMessages {
					if _,present := myself.Users[message.From]; !present {
						continue
					}
				}
				// TODO: if anon chan, match against regexp to see if there should be any rewrite
				for nickname, ircUser := range myself.Users {
					if nickname != message.From { 
						if myself.Anonymous && privmsgRegexp.Match(message.Message) {
							msg := bytes.SplitN(message.Message, []byte(":"), 3)
							if len(msg) == 3 {
								ircUser.rawOutput <- []byte(":anonymous!anonymous@" + server.Hostcloak + " PRIVMSG " + myself.Name + " :" + string(msg[2]))
							}
						} else {
							ircUser.rawOutput <- message.Message 
						}
					}
				}
			case req := <-myself.Operation :
				scanner := bufio.NewScanner(strings.NewReader(req.Command))
				scanner.Split(bufio.ScanWords)
				if !scanner.Scan() { panic(fmt.Sprintf("%#v\n", req)) }
				command := scanner.Text()
				if command == "JOIN" {
					if _,isBanned := myself.Banned[req.User.Nickname]; isBanned {
						req.ReplyTo <- []byte(ERR_BANNEDFROMCHAN(req.User.Nickname, myself.Name))
						continue
					}
					if myself.Key != "" {
						if !scanner.Scan() || scanner.Text() != myself.Key {
							req.ReplyTo <- []byte(ERR_BADCHANNELKEY(req.User.Nickname, myself.Name))
							continue
						}
					}
					// op the first user joining the channel, if this is an ordinary channel
					if myself.ChannelType() == '#' && len(myself.Users) == 0 {
						myself.Operators[req.User.Nickname] = req.User
					}
					myself.Users[req.User.Nickname] = req.User
					usersInChannelsMutex.Lock()
					usersInChannels[req.User.Nickname][myself.Name] = myself
					usersInChannelsMutex.Unlock()
					if !myself.Anonymous {
						// channel is not anonymous, so we dump the list of attendees
						nicknames := make([]string, len(myself.Users))
						i:=0
						for name := range myself.Users {
							if _,isOp := myself.Operators[name]; isOp {
								nicknames[i] = "@" + name
							} else {
								nicknames[i] = name
							}
							i++
						}
						req.ReplyTo <- []byte(
							JOIN(req.User.Nickname, myself.Name) + 
							RPL_NAMREPLY(req.User.Nickname, myself.Name, nicknames) +
							RPL_ENDOFNAMES(req.User.Nickname, myself.Name) + 
							RPL_MAYBETOPIC(req.User.Nickname, myself.Name, myself.Topic) +
							RPL_CHANNELMODEIS(req.User.Nickname, myself.Name, myself.ModeString()) )
						for _,ircUser := range myself.Users {
							if ircUser != req.User {
								ircUser.rawOutput <- []byte(JOIN(req.User.Nickname, myself.Name))
							}
						}
					} else {
						// channel is anonymous and so we censor the output here
						fakeList := make([]string, 2)
						fakeList[0] = "anonymous"
						fakeList[1] = req.User.Nickname
						req.ReplyTo <- []byte(
							JOIN(req.User.Nickname, myself.Name) +
							RPL_NAMREPLY(req.User.Nickname, myself.Name, fakeList) +
							RPL_ENDOFNAMES(req.User.Nickname, myself.Name) + 
							RPL_CHANNELMODEIS(req.User.Nickname, myself.Name, myself.ModeString()) +
							PRIVMSG("anonymous", myself.Name, "This channel is anonymous. Everyone in this channel will appear as me.") )
					}
				}
				if command == "PART" {
					if _,ok := myself.Users[req.User.Nickname]; !ok {
						req.ReplyTo <- []byte(ERR_NOTONCHANNEL(req.User.Nickname, myself.Name))
						continue
					}
					msg := []byte(PART(req.User.Nickname, myself.Name))
					delete(myself.Users, req.User.Nickname)
					delete(myself.Operators, req.User.Nickname)
					if !myself.Anonymous {
						for _,user := range myself.Users {
							if user != req.User {
								user.rawOutput <- msg
							}
						}
					}
					req.ReplyTo <- msg 			// this ...
												// has to happen after ...
					usersInChannelsMutex.Lock() // this.
					delete(usersInChannels[req.User.Nickname], myself.Name)
					usersInChannelsMutex.Unlock()
					if len(myself.Users) == 0 {
						return // when the last user leaves, the channel destroys itself
					}
				}
				if command == "TOPIC" {
					if _,ok := myself.Users[req.User.Nickname]; !ok { 
						req.ReplyTo <- []byte(ERR_NOTONCHANNEL(req.User.Nickname, myself.Name))
						continue
					}
					if !scanner.Scan() {
						// get topic
						req.ReplyTo <- []byte(RPL_MAYBETOPIC(req.User.Nickname, myself.Name, myself.Topic))
						continue
					} else {
						// set topic
						if myself.TopicLock {
							if _,present := myself.Operators[req.User.Nickname]; !present {
								req.ReplyTo <- []byte(ERR_CHANOPRIVSNEEDED(req.User.Nickname, myself.Name))
								continue
							}
						}
						newTopic := strings.SplitN(req.Command, " ", 2)[1]
						myself.Topic = newTopic
						for nickname,user := range myself.Users {
							user.rawOutput <- []byte(RPL_TOPIC(nickname, myself.Name, myself.Topic))
						}
					}
				}
				if command == "NICK" {
					/*
						Syntax: "NICK oldNick newNick"
						Replies with a list of users in the channel; Because each user should receive ONE nick-update
						meassage each, and as the ircUser iterates through all the channels, and updates his nick in
						each one of them, each other user that is in *any* of the channels the user is in shall receive
						exactly one nick update message...
					*/
					scanner.Scan()
					oldNick := scanner.Text()
					scanner.Scan()
					newName := scanner.Text()
					delete(myself.Users, oldNick)
					delete(myself.Operators, oldNick)
					myself.Users[newName] = req.User
					myself.Operators[newName] = req.User
					userList := ""
					for nickname,ircUser := range myself.Users {
						if req.User != ircUser {
							userList += nickname + " "
						}
					}
					req.ReplyTo <- []byte(userList)
				}
				if command == "MODE" {
					if !scanner.Scan() {
						// get mode
						req.ReplyTo <- []byte(RPL_CHANNELMODEIS(req.User.Nickname, myself.Name, myself.ModeString()))
						continue
					} else {
						// set mode, OR ask for extended information about the channel, which is covered in the switch-case below:
						newMode := strings.TrimSpace(strings.SplitN(req.Command, " ", 2)[1])
						if len(newMode) == 1 {
							switch newMode {
								case "b" :
									req.ReplyTo <- []byte(RPL_ENDOFBANLIST(req.User.Nickname, myself.Name))
									continue
								case "e" :
									req.ReplyTo <- []byte(RPL_ENDOFEXCEPTLIST(req.User.Nickname, myself.Name))
									continue
								case "I" :
									req.ReplyTo <- []byte(RPL_ENDOFINVITELIST(req.User.Nickname, myself.Name))
									continue
								case "O" :
									// this case appears not to be covered in RFC2812 /!\ weird
									req.ReplyTo <- []byte("DERP ERROR :DON'T ASK THAT QUESTION\r\n")
									continue
							}
						}
						// so if it was not one of those three weird cases, it most likely is a set mode-operation...
						if !req.User.IsOperator {
							if _,isOp := myself.Operators[req.User.Nickname]; !isOp {
								req.ReplyTo <- []byte(fmt.Sprintf("DERP %s\r\n", newMode) + ERR_CHANOPRIVSNEEDED(req.User.Nickname, myself.Name))
								continue
							}
						}
						if badChar,ok := myself.ModifyMode(newMode); !ok {
							if badChar != "\x00" {
								req.ReplyTo <- []byte(ERR_UNKNOWNMODE(req.User.Nickname, myself.Name, badChar))
							} else {
								req.ReplyTo <- []byte(ERR_NEEDMOREPARAMS(req.User.Nickname, command))
							}
							continue
						}
						msg := []byte(MODE(req.User.Nickname, myself.Name, newMode))
						for _,ircUser := range myself.Users {
							if ircUser != req.User {
								ircUser.rawOutput <- msg
							}
						}
						req.ReplyTo <- msg
					}
				}
				if command == "NAMES" {
					if _,present := myself.Users[req.User.Nickname]; !present {
						req.ReplyTo <- []byte("")
						continue
					}
					if myself.Anonymous {
						req.ReplyTo <- []byte(RPL_NAMREPLY(req.User.Nickname, myself.Name, []string{"anonymous", req.User.Nickname}) + RPL_ENDOFNAMES(req.User.Nickname, myself.Name))
						continue
					}
					userList := make([]string, len(myself.Users))
					i := 0
					for nick,_ := range myself.Users {
						if _,isOp := myself.Operators[nick]; isOp {
							userList[i] = "@" + nick
						} else {
							userList[i] = nick
						}
						i++
					}
					req.ReplyTo <- []byte(RPL_NAMREPLY(req.User.Nickname, myself.Name, userList) + RPL_ENDOFNAMES(req.User.Nickname, myself.Name))
				}
				if command == "LIST" {
					if _,present := myself.Users[req.User.Nickname]; !present {
						req.ReplyTo <- []byte("")
						continue
					}
					if myself.Anonymous {
						req.ReplyTo <- []byte(RPL_LIST(req.User.Nickname, myself.Name, myself.Topic, 9001))
					} else {
						req.ReplyTo <- []byte(RPL_LIST(req.User.Nickname, myself.Name, myself.Topic, len(myself.Users)))
					}
				}
				if command == "KICK" {
					// syntax: "KICK whom" OR "KICK whom :multi-word comment"
					if _,present := myself.Users[req.User.Nickname]; !present {
						req.ReplyTo <- []byte(ERR_NOTONCHANNEL(req.User.Nickname, myself.Name))
						continue
					}
					if _,isOp := myself.Operators[req.User.Nickname]; !isOp {
						req.ReplyTo <- []byte(ERR_CHANOPRIVSNEEDED(req.User.Nickname, myself.Name))
						continue
					}
					if !scanner.Scan() { panic("CHANNEL KICK without anyone to kick") }
					whom := scanner.Text()
					if _,present := myself.Users[whom]; !present {
						req.ReplyTo <- []byte(ERR_USERNOTINCHANNEL(req.User.Nickname, whom, myself.Name))
						continue
					}
					// extract comment if possible
					comment := ""
					coms := strings.SplitN(req.Command, ":", 2)
					if len(coms) == 2 {
						comment = coms[1]
					}
					delete(myself.Operators, whom)
					usersInChannelsMutex.Lock()
					delete(usersInChannels[whom], myself.Name)
					usersInChannelsMutex.Unlock()
					for _,ircUser := range myself.Users {
						ircUser.rawOutput <- []byte(KICK(req.User.Nickname, whom, myself.Name, comment))
					}
					delete(myself.Users, whom)
				}
				if command == "WHO" {
					// syntax: "WHO"
					if _,present := myself.Users[req.User.Nickname]; !present {
						req.ReplyTo <- []byte(RPL_ENDOFWHO(req.User.Nickname, myself.Name))
						continue
					}
					var rply []string
					if myself.Anonymous {
						rply = make([]string, 2)
						rply[0] = RPL_SIMPLEWHOREPLY(req.User.Nickname, myself.Name, "anonymous", false)
						rply[1] = RPL_SIMPLEWHOREPLY(req.User.Nickname, myself.Name, req.User.Nickname, myself.hasOp(req.User.Nickname))
					} else {
						rply = make([]string, len(myself.Users))
						i := 0
						for nick,_ := range myself.Users {
							rply[i] = RPL_SIMPLEWHOREPLY(req.User.Nickname, myself.Name, nick, myself.hasOp(nick))
							i++
						}
					}
					req.ReplyTo <- []byte(strings.Join(rply, "") + RPL_ENDOFWHO(req.User.Nickname, myself.Name))
				}
		}
	}
}


// returns true if user with that nickname has op
func (myself *IRCChannel) hasOp(nickname string) bool {
	_,hasOp := myself.Operators[nickname]
	return hasOp
}



func (myself *IRCChannel) SendMessage(from string, bytes []byte) bool {
	myself.Distribute <- &IRCDistribute{From: from, Message: bytes}
	return true
}



func (myself *IRCChannel) Identifier() string {
	return myself.Name
}



func (myself *IRCChannel) CleanDestroy() bool {
	usersInChannelsMutex.Lock()
	for _,user := range myself.Users { // this will probably be an empty list almost always, or always
		delete(usersInChannels[user.Nickname], myself.Name)
		user.rawOutput <- []byte(PART(user.Nickname, myself.Name))
	}
	usersInChannelsMutex.Unlock()

	reply := make(chan bool)
	request := &NSDelRequest{Key: myself.Name, ReplyTo: reply}
	nameSpace.Del <- request
	return <-reply
}



