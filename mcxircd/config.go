/*
	reads config files
	
	./mcxircd.conf	-	the master config file
	./motd			-	message of the day, raw IRC-format

*/
package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	)

var (
	defaultConfigMap = map[string]string {
		"fullname": "irc.mcxircd.derp",
		"shortname": "mcxircd",
		"hostcloak": "cloak",
		"software": "mcxircd",
		"version": "mcxircd-0",
		"quote": "Om mani padme hum",
		"defaultrealname": "Chelsea Manning",
		"maxnicklen": "9",
	}
	opersMap map[string]string = nil
	configMap map[string]string = nil
	motd [][]byte = nil
)

func readConfigFile(confFileName string) {
	configMap := make(map[string]string)
	for k,v := range defaultConfigMap {
		configMap[k]=v
	}
	b,e := ioutil.ReadFile(confFileName)
	if e!=nil {
		fmt.Println("No config file found. Falling back to default values:")
		for key,value := range configMap {
			fmt.Printf("\t%s = %s\n", key, value)
		}
		return
	}
	lineScanner := bufio.NewScanner(strings.NewReader(string(b)))
	for lineNum:=1; lineScanner.Scan(); lineNum++ {
		line := strings.TrimSpace(lineScanner.Text())
		if len(line) == 0 || ( len(line) > 0 && line[0] == '#' ) {
			// ignore empty lines and lines that begin with #
			continue
		}
		keyVal := strings.SplitN(line, "=", 2)
		if len(keyVal) == 2 {
			key := strings.ToLower(strings.TrimSpace(keyVal[0]))
			value := strings.TrimSpace(keyVal[1])
			if value[0] == '"' && value[len(value)-1] == '"' {
				value = value[1:len(value)-1] // remove dem quotes
			}
			delete(configMap, key)
			configMap[key]=value
		} else {
			badConfig(confFileName, lineNum)
		}
	}
	if v,p := configMap["fullname"]; p {
		server.FullName = v
	}
	if v,p := configMap["shortname"]; p {
		server.ShortName = v
	}
	if v,p := configMap["software"]; p {
		server.Software = v
	}
	if v,p := configMap["version"]; p {
		server.Version = v
	}
	if v,p := configMap["hostcloak"]; p {
		server.Hostcloak = v
	}
	if v,p := configMap["quote"]; p {
		server.Quote = v
	}
	if v,p := configMap["defaultrealname"]; p {
		server.DefaultRealName = v
	}
	if v,p := configMap["maxnicklen"]; p {
		if vi,e := strconv.Atoi(v); e!=nil {
			fmt.Printf("%s is not a number\n", v)
			badConfig(confFileName, -1)
		} else {
			server.MaxNickLen = vi
		}
	}
}

func readMOTDFile(motdFileName string) {
	b,e := ioutil.ReadFile(motdFileName)
	if e!=nil {
		fmt.Println("No MOTD file found. Falling back on default prayer.")
		return
	}
	motd = bytes.Split(b, []byte{'\n'})
}

func readOPERSFile(opersFileName string) {
	opersMap = make(map[string]string)
	b,e := ioutil.ReadFile(opersFileName)
	if e!=nil {
		fmt.Println("No OPERS file found. No-one will be operator.")
		return
	}
	lineScanner := bufio.NewScanner(strings.NewReader(string(b)))
	for lineNum:=1; lineScanner.Scan(); lineNum++ {
		line := strings.TrimSpace(lineScanner.Text())
		if len(line) == 0 || ( len(line) > 0 && line[0] == '#' ) {
			// ignore empty lines and lines that begin with #
			continue
		}
		keyVal := strings.SplitN(line, " ", 2)
		if len(keyVal) != 2 { badConfig(opersFileName, lineNum) }
		opersMap[keyVal[0]]=keyVal[1]
	}
}

func badConfig(filename string, lineNum int) {
	fmt.Printf("Config error in file %s, line %d, aborting.\n", filename, lineNum)
	os.Exit(1)
}



