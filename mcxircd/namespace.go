/*
 * Namespace is a singleton object that has a map[string]NamedIRCItem, that 
 * contains all named items in the known IRC-world. That is, users and channels.
 * Whenever anything needs to send a message to something else, and it does not
 * have a direct pointer to use, it looks it up using the namespace.
 *
 * Maybe this would be more effective with a mutex. Reads happen a lot more 
 * often, and multiple reads can happen simultaneously.
 *
 */
package main 

import (
)

var nameSpace NameSpace

type NamedIRCItem interface {
	Identifier()				string
	SendMessage(string, []byte)	bool
	CleanDestroy()				bool
}

type NSLookupRequest struct {
	Key			string
	ReplyTo		chan(NamedIRCItem)
}

type NSAddRequest struct {
	Key			string
	Value		NamedIRCItem
	ReplyTo		chan(bool)
}

type NSDelRequest struct {
	Key			string
	ReplyTo		chan(bool)
}

type NSReplaceRequest struct {
	Key			string
	NewKey		string
	ReplyTo		chan(bool)
}

type NameSpace struct {
	Lookup		chan(*NSLookupRequest)
	Add			chan(*NSAddRequest)
	Del			chan(*NSDelRequest)
	Replace		chan(*NSReplaceRequest)
	Map			map[string]NamedIRCItem
}

func lookuper(ns *NameSpace) {
	for {
		select {
			case req := <-ns.Lookup :
				r := ns.Map[req.Key]
				req.ReplyTo <- r
			case req := <-ns.Add :
				if ns.Map[req.Key] != nil {
					req.ReplyTo <- false
				} else {
					ns.Map[req.Key] = req.Value
					req.ReplyTo <- true
				}
			case req := <-ns.Del :
				if ns.Map[req.Key] == nil {
					req.ReplyTo <- false
				} else {
					delete(ns.Map, req.Key)
					req.ReplyTo <- true
				}
			case req := <-ns.Replace :
				if value,ok := ns.Map[req.Key]; !ok {
					req.ReplyTo <- false
				} else {
					delete(ns.Map, req.Key)
					ns.Map[req.NewKey] = value
					req.ReplyTo <- true
				}
		}
	}
}

func makeNameSpace() *NameSpace {
	t := NameSpace{
		Lookup: make(chan(*NSLookupRequest)),
		Add: make(chan(*NSAddRequest)),
		Del: make(chan(*NSDelRequest)),
		Replace: make(chan(*NSReplaceRequest)),
		Map: make(map[string]NamedIRCItem) }
	go lookuper(&t)
	return &t
}

