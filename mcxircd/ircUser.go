/*
 * An IRCUser is an object that consists of an IRCUser struct, and two 
 * goroutines: One for parsing input generated by the user (mosly blocked on 
 * socket read), and one for writing messages to the user and also handling 
 * message passing (almost never blocks).
 */
package main



import (
	"net"
	"fmt"
	"time"
	"bufio"
	"strings"
	"regexp"
	)

var (
	validUsername string = "[a-zA-Z\\[\\]\\\\`_\\^\\{\\}][a-zA-Z0-9\\-\\[\\]\\\\`_\\^\\{\\}]*"
	validUsernameRegexp *regexp.Regexp = regexp.MustCompile("^" + validUsername + "$")
	)


// An IRCUser is an object that consists of this structure, and two goroutines
type IRCUser struct {
	Nickname		string
	Conn			net.Conn
	waitingForPong  	bool
	Timeout			time.Time
	pong			chan(bool)
	updateTimeout		chan(bool)
	rawOutput		chan([]byte)
	setMode			chan(*UserModeUpdate)
	Die				chan(bool)
	Kill			chan(bool)

	// user modes
	IsOperator		bool
	Away			bool
	Invisible		bool
	Wallops			bool
	Restricted		bool
	ModeS			bool
}

// are sent to users when their modes change
type UserModeUpdate struct {
	User			*IRCUser
	ModeCommand		string
	ReplyTo			chan(string)
}



// Creates a new IRCUser
func newIRCUser(conn net.Conn, nickname string) (*IRCUser, bool) {
	if !validUsernameRegexp.MatchString(nickname) || len(nickname) > server.MaxNickLen || nickname == "" { 
		return nil, false 
	}
	if nickname == "anonymous" { 
		return nil, false 
	}

	myself := IRCUser{Nickname: nickname, Conn: conn, waitingForPong: false, IsOperator: false}
	myself.Timeout = time.Now().Add(TIMEOUT_PING * time.Second);
	
	myself.pong = make(chan bool)
	myself.rawOutput = make(chan []byte)
	myself.updateTimeout = make(chan bool)
	myself.setMode = make(chan *UserModeUpdate)
	myself.Die = make(chan bool)
	myself.Kill = make(chan bool)

	nameOK := make(chan bool)
	nameSpace.Add <- &NSAddRequest{Key: nickname, Value: &myself, ReplyTo: nameOK}
	ok := <-nameOK
	if ok {
		usersInChannelsMutex.Lock()
		usersInChannels[nickname] = make(map[string]*IRCChannel)
		usersInChannelsMutex.Unlock()
		
		go myself.ircUserLoop()
		go myself.ircUserMessenger()
	}
	return &myself, ok
}



// Executes as a goroutine, and is very tightly coupled with ircUserLoop and IRCUser. This handles
// most message passing with other users and channels (that is, "chatting"). It also handles sending
// PINGs, if the user has not said anything for a while.
func (myself *IRCUser) ircUserMessenger() {
	defer myself.Conn.Close()
	
	for {
		timeoutIntervall := myself.Timeout.Sub(time.Now())
		select {
			case <-myself.pong :
				myself.Timeout = time.Now().Add(TIMEOUT_PING * time.Second);
				myself.waitingForPong = false

			case <-myself.updateTimeout :
				if !myself.waitingForPong {
					myself.Timeout = time.Now().Add(TIMEOUT_PING * time.Second);
				}

			case req := <-myself.setMode :
				req.ReplyTo <- myself.updateMode(req.User, req.ModeCommand)

			case rawbytes := <-myself.rawOutput :
				if !badCTCP(string(rawbytes)) {
					myself.Conn.Write(rawbytes)
				}
				
			case <-time.After(timeoutIntervall) :
				if myself.waitingForPong {
					myself.Conn.Write([]byte("ERROR :PING TIMEOUT\r\n"))
					return
				} else {
					myself.waitingForPong = true
					myself.Conn.Write([]byte("PING\r\n"))
					myself.Timeout = time.Now().Add(TIMEOUT_PONG * time.Second);
				}

			// used to kill this goroutine, which in turn causes the other 
			// goroutine to also die, unless it has not died already.
			case <-myself.Die :
				return

			// kills the other goroutine, and waits for it to kill this 
			// goroutine. used when someone else than this IRC user kills this
			// IRC user.
			case <-myself.Kill :
				myself.Conn.Close()

		}
	}
}

// updates the users mode according to the modeCommand. See RFC2812 section 
// 3.1.5 for more information. this replies with the message that results from
// applying a MODE command on a user.
func (myself *IRCUser) updateMode(user *IRCUser, modeCommand string) string {
	IsOperator := myself.IsOperator
	Away := myself.Away
	Invisible := myself.Invisible
	Wallops := myself.Wallops
	Restricted := myself.Restricted
	ModeS := myself.ModeS
	mod := true

	words := bufio.NewScanner(strings.NewReader(modeCommand))
	words.Split(bufio.ScanWords)
	for words.Scan() {
		s := bufio.NewScanner(strings.NewReader(words.Text()))
		s.Split(bufio.ScanRunes)
		for s.Scan() {
			r := rune(s.Text()[0])
			switch r {
				case '+' :
					mod = true
				case '-' :
					mod = false
				case 'a' :
					Away = mod
				case 'i' :
					Invisible = mod
				case 'w' :
					Wallops = mod
				case 's' :
					ModeS = mod
				case 'o' :
					if user.IsOperator {
						IsOperator = mod
					} else {
						return ERR_NOPRIVILEGES(user.Nickname)
					}
				default :
					return ERR_UMODEUNKNOWNFLAG(user.Nickname)
			}
		}
		if err := s.Err(); err != nil {
			return ERR_NEEDMOREPARAMS(user.Nickname, "MODE")
		}
	}
	if err := words.Err(); err != nil {
		return ERR_NEEDMOREPARAMS(user.Nickname, "MODE")
	}
	myself.IsOperator = IsOperator
	myself.Away = Away
	myself.Invisible = Invisible
	myself.Wallops = Wallops
	myself.Restricted = Restricted
	myself.ModeS = ModeS
	return RPL_UMODEIS(myself.Nickname, myself.ModeString())
}

func (myself *IRCUser) ModeString() string {
	str := "+"
	if myself.Away { str += "a" }
	if myself.Invisible { str += "i" }
	if myself.Wallops { str += "w" }
	if myself.IsOperator { str += "o" }
	if myself.Restricted { str += "r" }
	if myself.ModeS { str += "s" }
	return str
}




// Executes as a goroutine. It says "HELLO!" to the user and then goes into an 
// (almost) eternal loop, where it parses user input from the associated socket,
// and tries to be as RFC2812-compliant (RTFM) as possible.
func (myself *IRCUser) ircUserLoop() {
	defer myself.CleanDestroy()
	defer myself.Conn.Close()

	// hello and welcome!
	myself.rawOutput <- []byte(
		RPL_WELCOME(myself.Nickname) +
		RPL_YOURHOST(myself.Nickname) +
		RPL_CREATED(myself.Nickname) + 
		RPL_MYINFO(myself.Nickname) + 
		RPL_MOTDSTART(myself.Nickname) + 
		RPL_MOTD(myself.Nickname) + 
		RPL_ENDOFMOTD(myself.Nickname) )

	// go into a loop that blocks on reading from the socket
	scanner := bufio.NewScanner(myself.Conn) // splits input at \r\n OR \n
	for {
		if !scanner.Scan() {
			//fmt.Printf("%s died because %v", myself.Nickname, scanner.Err())
			return
		}
		
		line := scanner.Text()
		wordScanner := bufio.NewScanner(strings.NewReader(line))
		cutter := makeScanWordsOrCUT()
		wordScanner.Split(cutter.ScanWordsOrCUT)
		if !wordScanner.Scan() {
			continue
		}
		command := wordScanner.Text()

		myself.updateTimeout <- true

		
/*
		 ____   ____   ____      _____   ____  ______ 
		|    \ |    | /    |    |     | /    ||      |
		|  o  ) |  | |   __|    |   __||  o  ||      |
		|     | |  | |  |  |    |  |_  |     ||_|  |_|
		|  O  | |  | |  |_ |    |   _] |  _  |  |  |  
		|     | |  | |     |    |  |   |  |  |  |  |  
		|_____||____||___,_|    |__|   |__|__|  |__|  
				                                      
		  _____ __    __  ____  ______   __  __ __    
		 / ___/|  |__|  ||    ||      | /  ]|  |  |   
		(   \_ |  |  |  | |  | |      |/  / |  |  |   
		 \__  ||  |  |  | |  | |_|  |_/  /  |  _  |   
		 /  \ ||  `  '  | |  |   |  |/   \_ |  |  |   
		 \    | \      /  |  |   |  |\     ||  |  |   
		  \___|  \_/\_/  |____|  |__| \____||__|__|
		  
		  ...that interprets incomming commands from connected users such
		  that whatever makes IRC tick, tick.
		  Essentially, this switch is an implementation of RFC2812.
		  https://www.rfc-editor.org/rfc/rfc2812.txt
*/

		switch strings.ToUpper(command) {
		
			case "PING" :
				myself.rawOutput <- []byte("PONG" + line[4:] + "\r\n")
				
			case "PONG" :
				myself.pong <- true
				
			case "HELP" :
				myself.rawOutput <- []byte("NO HALP!\r\n")
				
			case "QUIT" :
				myself.rawOutput <- []byte("ERROR :<3<3<3<4<5<6<7\r\n")
				return
				
			case "PRIVMSG", "NOTICE", "SQUERY" :
				if !wordScanner.Scan() { 
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "PRIVMSG"))
				}		
				if badCTCP(line) { 
					myself.rawOutput <- []byte("INFO: Your CTCP command was dropped: " + line + "\r\n") 
					continue
				}
				target := wordScanner.Text()
				c := make(chan(NamedIRCItem))
				nameSpace.Lookup <- &NSLookupRequest{Key: target, ReplyTo: c}
				receptor := <-c
				if receptor == nil {
					myself.rawOutput <- []byte("No such user\r\n")
					continue
				}
				if msgs := strings.SplitN(line, ":", 2); len(msgs) != 2 {
					myself.rawOutput <- []byte(syntaxError(line))
					continue
				} else {
					receptor.SendMessage(myself.Nickname, []byte(PRIVMSG(myself.Nickname, target, msgs[1])))
				}
				
			case "PART" :
				if !wordScanner.Scan() { 
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "PART"))
					continue
				}
				channelList := wordScanner.Text()
				channelSlice := strings.SplitN(channelList, ",", 21)
				if len(channelSlice) >= 20 {
					myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, channelList))
					continue
				}
				for _, target := range channelSlice {
					usersInChannelsMutex.RLock()
					c, ok := usersInChannels[myself.Nickname][target]
					usersInChannelsMutex.RUnlock()
					if !ok {
						myself.rawOutput <- []byte(ERR_NOTONCHANNEL(myself.Nickname, target))
					} else {
						c.Operation <- &IRCChanOp{myself, "PART", myself.rawOutput}
					}
				}
				
			case "JOIN" : // lenient and forgiving interpretation of RFC2812 §3.2.1
				var ok bool
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "JOIN"))
					continue
				}
				c := make(chan(NamedIRCItem))
				channelList := wordScanner.Text()
				if strings.EqualFold(channelList, "0") {
					// the "part from all channels-command"
					usersInChannelsMutex.Lock()
					channelsIamIn := make([]*IRCChannel, len(usersInChannels[myself.Nickname]))
					i := 0
					for _, channel := range usersInChannels[myself.Nickname] {
						channelsIamIn[i] = channel; i++
					}
					usersInChannelsMutex.Unlock()
					for _, channel := range channelsIamIn {
						channel.Operation <- &IRCChanOp{User: myself, Command: "PART", ReplyTo: myself.rawOutput }
					}
					continue
				}
				// slice of all channels to join
				channelSlice := strings.SplitN(channelList, ",", 21)
				if len(channelSlice) >= 20 {
					myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, channelList))
					continue
				}
				// Slice of keys. As we iterate through the list of channels to join, we shall also
				// iterate through the list of channel keys, and use each key as a password to the
				// channel about to be joined. The list of keys may be shorter than the list of
				// channels to be joined, in which case the remaining list of channels will be 
				// joined without using keys.
				keySlice := make([]string, 0)
				if wordScanner.Scan() {
					keySlice = strings.SplitN(wordScanner.Text(), ",", 21)
					if len(keySlice) >= 20 {
						myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, channelList))
						continue
					}
				}
				for i, target := range channelSlice {
					nameSpace.Lookup <- &NSLookupRequest{Key: target, ReplyTo: c}
					receptor := <-c
					if receptor == nil {
						receptor, ok = createIRCChannel(target)
						if !ok {
							myself.rawOutput <- []byte(ERR_NOSUCHCHANNEL(myself.Nickname, target))
							continue
						}
					}
					joinCommand := "JOIN"
					if len(keySlice) > i {
						joinCommand += " " + keySlice[i]
					}
					switch t := receptor.(type) {
						case *IRCChannel :
							t.Operation <- &IRCChanOp{myself, joinCommand, myself.rawOutput}
						default :
							myself.rawOutput <- []byte(ERR_NOSUCHCHANNEL(myself.Nickname, target))
					}
				}
				
			case "TOPIC" :
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "TOPIC"))
					continue
				}
				channel := wordScanner.Text()
				c := make(chan(NamedIRCItem))
				nameSpace.Lookup <- &NSLookupRequest{Key: channel, ReplyTo: c}
				receptor := <-c
				switch t := receptor.(type) {
					case *IRCChannel :
						if commandAndComment := strings.SplitN(line, ":", 2); len(commandAndComment) != 1 {
							t.Operation <- &IRCChanOp{User: myself, Command: "TOPIC " + commandAndComment[1], ReplyTo: myself.rawOutput}
						} else {
							t.Operation <- &IRCChanOp{User: myself, Command: "TOPIC", ReplyTo: myself.rawOutput}
						}
					default :
						myself.rawOutput <- []byte(ERR_NOSUCHCHANNEL(myself.Nickname, channel))
				}
				
			case "NICK" :
				// -1. is the nickname valid?
				// 0. is desired nickname already taken? if not, take a mutex and...
				// 1. update the usersInChannels map
				// 2. update the userlist in each channel the user is in
				// 3. make a set of all users in each channel the user is in,
				//    where each user only occurs once, to avoid sending too 
				//    many messages (see 4)
				// 4. send nick update messages to each user in that set
				// 5. update the namespace and release the mutex (see above)
				// 6. send nick update message to this user
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "NICK"))
					continue
				}
				desiredNick := wordScanner.Text()
				if !validUsernameRegexp.MatchString(desiredNick) {
					myself.rawOutput <- []byte(ERR_ERRONEUSNICKNAME(myself.Nickname, desiredNick))
					continue
				}
				if desiredNick == "anonymous" { 
					myself.rawOutput <- []byte(ERR_NICKNAMEINUSE(myself.Nickname, desiredNick))
					continue
				}
				cLookup := make(chan(NamedIRCItem))
				nameSpace.Lookup <- &NSLookupRequest{Key: desiredNick, ReplyTo: cLookup}
				maybeSomeone := <-cLookup
				if maybeSomeone != nil {
					myself.rawOutput <- []byte(ERR_NICKNAMEINUSE(myself.Nickname, desiredNick))
					continue
				}
				usersInChannelsMutex.Lock()
				myChannelMap := usersInChannels[myself.Nickname]
				usersInChannels[desiredNick] = myChannelMap
				oldNick := myself.Nickname
				myself.Nickname = desiredNick
				delete(usersInChannels, oldNick)
				userList := ""
				cBytes := make(chan []byte)
				for _,ircChannel := range usersInChannels[myself.Nickname] {
					ircChannel.Operation <- &IRCChanOp{User: myself, Command: "NICK " + oldNick + " " + desiredNick, ReplyTo: cBytes}
					userList += string(<-cBytes)
				}
				strings.TrimSpace(userList)
				if userList != "" {
					userSet := make(map[string]bool)
					for _,name := range strings.Split(userList, " ") {
						userSet[name]=true
					}
					for name,_ := range userSet {
						nameSpace.Lookup <- &NSLookupRequest{Key: name, ReplyTo: cLookup}
						ircUser := <-cLookup
						if ircUser != nil {
							ircUser.SendMessage(myself.Nickname, []byte(NICK(oldNick, desiredNick)))
						}
					}
				}
				cOK := make(chan bool)
				nameSpace.Replace <- &NSReplaceRequest{Key: oldNick, NewKey: desiredNick, ReplyTo: cOK}
				<-cOK
				usersInChannelsMutex.Unlock()
				myself.rawOutput <- []byte(NICK(oldNick, desiredNick))
				
			case "MODE" :
				// handles both channel and user mode; both requests and attempts at modifications
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "MODE"))
					continue
				}
				target := wordScanner.Text()
				cLookup := make(chan(NamedIRCItem))
				nameSpace.Lookup <- &NSLookupRequest{Key: target, ReplyTo: cLookup}
				modeCommand := ""
				for wordScanner.Scan() {
					modeCommand += wordScanner.Text() + " "
				}
				strings.TrimSpace(modeCommand)
				switch t := (<-cLookup).(type) {
					case *IRCUser :
						if modeCommand == "" {
							myself.rawOutput <- []byte(RPL_UMODEIS(myself.Nickname, myself.ModeString()))
						} else {
							if t != myself && !myself.IsOperator {
								myself.rawOutput <- []byte(ERR_USERSDONTMATCH(myself.Nickname))
								continue
							}
							// send message to user, to change mode
							cR := make(chan string)
							t.setMode <- &UserModeUpdate{User: myself, ModeCommand: modeCommand, ReplyTo: cR}
							modeMsg := <-cR
							myself.rawOutput <- []byte(modeMsg)
							if t != myself {
								t.rawOutput <- []byte(MODE(myself.Nickname, t.Nickname, modeCommand) + modeMsg)
							}
						}
					case *IRCChannel :
						if modeCommand == "" {
							t.Operation <- &IRCChanOp{User: myself, Command: "MODE", ReplyTo: myself.rawOutput}
						} else {
							t.Operation <- &IRCChanOp{User: myself, Command: "MODE " + modeCommand, ReplyTo: myself.rawOutput}
						}
					default :
						myself.rawOutput <- []byte(ERR_NOSUCHNICK(myself.Nickname, target))
				}
				
			case "NAMES" :
				if !wordScanner.Scan() {
					// user asks for a list of *all* users and channels. reply with just the anonymous pseudo-user.
					usersInChannelsMutex.RLock()
					for _,ircChan := range usersInChannels[myself.Nickname] {
						ircChan.Operation <- &IRCChanOp{User: myself, Command: "NAMES", ReplyTo: myself.rawOutput}
					}
					usersInChannelsMutex.RUnlock()
					myself.rawOutput <- []byte(RPL_NAMREPLY(myself.Nickname, "*", []string{"anonymous"}) + RPL_ENDOFNAMES(myself.Nickname, "*"))
					continue
				}
				targetList := wordScanner.Text()
				if wordScanner.Scan() && wordScanner.Text() != server.FullName {
					// do not forward this message to any other server
					myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
					continue
				}
				channelSlice := strings.SplitN(targetList, ",", 21)
				if len(channelSlice) > 20 {
					myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, targetList))
					continue
				}
				usersInChannelsMutex.RLock()
				myChannels := usersInChannels[myself.Nickname]
				for _,name := range channelSlice {
					if ircChan,ok := myChannels[name]; ok {
						ircChan.Operation <- &IRCChanOp{User: myself, Command: "NAMES", ReplyTo: myself.rawOutput}
					}
				}
				usersInChannelsMutex.RUnlock()

			case "LIST" :
				rBytesChan := make(chan []byte)
				if !wordScanner.Scan() {
					// user asks for a list of *all* channels. Reply with just those that the user is in.
					usersInChannelsMutex.RLock()
					for _,ircChan := range usersInChannels[myself.Nickname] {
						ircChan.Operation <- &IRCChanOp{User: myself, Command: "LIST", ReplyTo: rBytesChan}
						myself.rawOutput <- <-rBytesChan
					}
					usersInChannelsMutex.RUnlock()
					myself.rawOutput <- []byte(RPL_LISTEND(myself.Nickname))
					continue
				}
				chanList := wordScanner.Text()
				if wordScanner.Scan() && wordScanner.Text() != server.FullName {
					// do not forward this message to any other server
					myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
					continue
				}
				chanSlice := strings.SplitN(chanList, ",", 21)
				if len(chanSlice) > 20 {
					// actually ERR_TOOMANYMATCHES but that msg is not described in the RFC (DERP!?)
					myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, chanList))
					continue
				}
				usersInChannelsMutex.RLock()
				myChannels := usersInChannels[myself.Nickname]
				for _,name := range chanSlice {
					if ircChan,ok := myChannels[name]; ok {
						ircChan.Operation <- &IRCChanOp{User: myself, Command: "LIST", ReplyTo: rBytesChan}
						myself.rawOutput <- <-rBytesChan
					}
				}
				usersInChannelsMutex.RUnlock()
				myself.rawOutput <- []byte(RPL_LISTEND(myself.Nickname))
				
			case "KICK" :
				// The syntax of this command is utterly retarded. See RFC2812 §3.2.8.
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "KICK"))
					continue
				}
				chanList := wordScanner.Text()
				channelSlice := strings.SplitN(chanList, ",", 21)
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "KICK"))
					continue
				}
				userSlice := strings.SplitN(wordScanner.Text(), ",", 21)

				// decode kick comment if possible, using the special scanner-cutter
				comment := ""
				if wordScanner.Scan() { 
					comment = string(cutter.CUT())
				}
				
				if len(userSlice) > 1 && len(channelSlice) > 1 && len(userSlice) != len(channelSlice) {
					myself.rawOutput <- []byte(syntaxError(line))
					continue
				}
				if len(channelSlice) > 20 || len(userSlice) > 20 {
					myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, chanList))
					continue
				}
				if len(channelSlice) > 1 { 
					// Backwards compability case: The length of the channel list and the user list is equal
					for i,channel := range channelSlice {
						usersInChannelsMutex.RLock()
						if ircChan,present := usersInChannels[myself.Nickname][channel]; present {
							usersInChannelsMutex.RUnlock()
							kickCommand := "KICK " + userSlice[i]
							if comment != "" { kickCommand += " :" + comment }
							ircChan.Operation <- &IRCChanOp{User: myself, Command: kickCommand, ReplyTo: myself.rawOutput}
						} else {
							myself.rawOutput <- []byte(ERR_NOTONCHANNEL(myself.Nickname, channel))
						}
					}
				} else {
					// The normal case: One or multiple douchebags needs to be evicted from a channel
					usersInChannelsMutex.RLock()
					ircChan,present := usersInChannels[myself.Nickname][channelSlice[0]]
					usersInChannelsMutex.RUnlock()
					if !present {
						myself.rawOutput <- []byte(ERR_NOTONCHANNEL(myself.Nickname, channelSlice[0]))
						continue
					} else {
						for _,douchebag := range userSlice {
							kickCommand := "KICK " + douchebag
							if comment != "" { kickCommand += " :" + comment }
							ircChan.Operation <- &IRCChanOp{User: myself, Command: kickCommand, ReplyTo: myself.rawOutput}
						}
					}
				}
				
			case "INFO" :
				// According to RFC2812 §3.4.10, we are REQUIRED to implement this command.
				if wordScanner.Scan() && wordScanner.Text() != server.FullName {
					// do not forward this message to any other server
					myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
					continue
				}
				myself.rawOutput <- []byte(RPL_INFO(myself.Nickname) + RPL_ENDOFINFO(myself.Nickname))

			case "MOTD" :
				if wordScanner.Scan() && wordScanner.Text() != server.FullName {
					// do not forward this message to any other server
					myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
					continue
				}
				myself.rawOutput <- []byte(RPL_MOTDSTART(myself.Nickname) + RPL_MOTD(myself.Nickname) + RPL_ENDOFMOTD(myself.Nickname))

			case "WHO" :
				// Some IRC clients use WHO to get a list of everyone in a channel.
				if !wordScanner.Scan() || wordScanner.Text() == "0" {
					// user asks for a list of *all* users logged in. we return an empty list.
					myself.rawOutput <- []byte(RPL_ENDOFWHO(myself.Nickname, "*"))
					continue
				}
				target := wordScanner.Text()
				if wordScanner.Scan() {
					if wordScanner.Text() == "o" {
						// User asks to see opers only. Do not comply.
						myself.rawOutput <- []byte(RPL_ENDOFWHO(myself.Nickname, target))
						continue
					} else {
						myself.rawOutput <- []byte(syntaxError(line))
						continue
					}
				}
				cLookup := make(chan(NamedIRCItem))
				nameSpace.Lookup <- &NSLookupRequest{Key: target, ReplyTo: cLookup}
				switch t := (<-cLookup).(type) {
					case *IRCChannel :
						t.Operation <- &IRCChanOp{User: myself, Command: "WHO", ReplyTo: myself.rawOutput}
					case *IRCUser :
						myself.rawOutput <- []byte(RPL_SIMPLEWHOREPLY(myself.Nickname, "*", target, false) + RPL_ENDOFWHO(myself.Nickname, target))
					default :
						myself.rawOutput <- []byte(RPL_ENDOFWHO(myself.Nickname, target))
				}
				
			case "VERSION" :
				if wordScanner.Scan() && wordScanner.Text() != server.FullName {
					myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, wordScanner.Text()))
					if wordScanner.Scan() {
						myself.rawOutput <- []byte(syntaxError(line))
					}
					continue
				}
				myself.rawOutput <- []byte(RPL_VERSION(myself.Nickname))
				
			case "WHOIS" :
				remoteServer := ""
				whoList := ""
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(syntaxError(line))
					continue
				}
				whoList = wordScanner.Text()
				if wordScanner.Scan() {
					remoteServer = whoList
					whoList = wordScanner.Text()
					if wordScanner.Scan() {
						// 3rd argument? not allowed!
						myself.rawOutput <- []byte(syntaxError(line))
						continue
					}
				}
				if whoList == "*" || remoteServer == "*" {
					myself.rawOutput <- []byte(RPL_ENDOFWHOIS(myself.Nickname, "*"))
					continue
				}
				if remoteServer != "" && remoteServer != server.FullName {
					myself.rawOutput <- []byte(ERR_NOSUCHSERVER(myself.Nickname, remoteServer))
					continue
				}
				whoSlice := strings.SplitN(whoList, ",", 21)
				if len(whoSlice) > 20 {
					myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, whoList))
					continue
				}
				cLookup := make(chan(NamedIRCItem))
				for i:=0; i!=len(whoSlice); i++ {
					nameSpace.Lookup <- &NSLookupRequest{Key: whoSlice[i], ReplyTo: cLookup}
					switch (<-cLookup).(type) {
						case *IRCUser :
							myself.rawOutput <- []byte(RPL_SIMPLEWHOISUSER(myself.Nickname, whoSlice[i]) + RPL_ENDOFWHOIS(myself.Nickname, whoSlice[i]))
						default :
							myself.rawOutput <- []byte(ERR_NOSUCHNICK(myself.Nickname, whoSlice[i]) + RPL_ENDOFWHOIS(myself.Nickname, whoSlice[i]))
					}
				}
			case "WHOWAS" :
				// always reply with "there never was no such a user", no matter what the input is.
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "WHOWAS"))
					continue
				}
				whoSlice := strings.SplitN(wordScanner.Text(), ",", 21)
				if len(whoSlice) > 20 {
					myself.rawOutput <- []byte(ERR_TOOMANYTARGETS(myself.Nickname, strings.Join(whoSlice, " ")))
					continue
				}
				for _,nick := range whoSlice {
					myself.rawOutput <- []byte(ERR_WASNOSUCHNICK(myself.Nickname, nick))
				}

			case "OPER" :
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "OPER"))
					continue
				}
				username := wordScanner.Text()
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "OPER"))
					continue
				}
				password := wordScanner.Text()
				if realPass,exist := opersMap[username]; exist {
					if realPass == password {
						myself.IsOperator = true
						myself.rawOutput <- []byte(RPL_YOUREOPER(myself.Nickname) + MODE("anonymous", myself.Nickname, "+o") + RPL_UMODEIS(myself.Nickname, myself.ModeString()))
						continue
					}
				}
				myself.rawOutput <- []byte(ERR_PASSWDMISMATCH(myself.Nickname))

			case "KILL" :
				if !myself.IsOperator {
					myself.rawOutput <- []byte(ERR_NOPRIVILEGES(myself.Nickname))
					continue
				}
				if !wordScanner.Scan() {
					myself.rawOutput <- []byte(ERR_NEEDMOREPARAMS(myself.Nickname, "OPER"))
					continue
				}
				whomToKill := wordScanner.Text()
				cLookup := make(chan(NamedIRCItem))
				nameSpace.Lookup <- &NSLookupRequest{Key: whomToKill, ReplyTo: cLookup}
				switch t := (<-cLookup).(type) {
					case *IRCUser :
						t.Kill <- true
					default :
						myself.rawOutput <- []byte(ERR_NOSUCHNICK(myself.Nickname, whomToKill))
				}

			default :
				myself.rawOutput <- []byte(syntaxError(line))
		}
	}
}



func syntaxError(line string) string {
	return fmt.Sprintf("ERROR :Bad input or unimplemented. You said: " + line + "\r\n")
}



// returns true if the IRC command is a sensetive CTCP message. such messages 
// can be used to reveal unnecessarily much about the users identity.
func badCTCP(message string) bool {
	if len(message) == 0 { return false }
	if privmsgORnotice.MatchString(message) {
		splitStrs := strings.SplitN(message, ":", 3)
		var msg string
		if len(splitStrs) == 3 && message[0] == ':' { 
			msg = splitStrs[2] 
		} else if len(splitStrs) == 2 && len(splitStrs[0]) > 0 && message[0] != ':' { 
			msg = splitStrs[1] 
		} else {
			return false
		}
		if len(msg) > 0 && msg[0] == '\x01' {
			// its a CTCP message
			if strings.EqualFold(firstSequenceOfLetters(msg[1:]), "ACTION") {
				return false
			}
			// its a CTCP that is not a /me (ACTION)
			return true
		}
	}
	return false
}
var privmsgORnotice *regexp.Regexp = regexp.MustCompile(`^(\S* )? *PRIVMSG|NOTICE|SQUERY \S* :`)



func (myself *IRCUser) SendMessage(from string, bytes []byte) bool {
	myself.rawOutput <- bytes
	return true
}



func (myself *IRCUser) Identifier() string {
	return myself.Nickname
}



func (myself *IRCUser) CleanDestroy() bool {

	// make list of channels I am in
	usersInChannelsMutex.RLock()
	myChans := make([]*IRCChannel, len(usersInChannels[myself.Nickname]))
	i:=0
	for _, ircChan := range usersInChannels[myself.Nickname] {
		myChans[i] = ircChan
		i++
	}
	usersInChannelsMutex.RUnlock()

	// part from all those channels (see above)
	replyBytes := make(chan []byte)
	for _, ircChan := range myChans {
		ircChan.Operation <- &IRCChanOp{User: myself, Command: "PART", ReplyTo: replyBytes}
		<-replyBytes
	}

	// remove my name from the map of users->channels
	usersInChannelsMutex.Lock()
	delete(usersInChannels, myself.Nickname)
	usersInChannelsMutex.Unlock()

	// kill the message-passing goroutine
	myself.Die <- true

	// delete my name from the namespace
	reply := make(chan bool)
	nameSpace.Del <- &NSDelRequest{Key: myself.Nickname, ReplyTo: reply}
	return <-reply
}



