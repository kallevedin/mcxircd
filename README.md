MCXIRCD
=======

Yet another IRC server, this time written in go!

It is a fully working server. It lacks some functionality, such as 
TLS-encryption and it can not connect with other servers to form IRC networks.
Most commands are implemented. Just like the AnonIRCd, MCXIRCD is targeted for
TOR/I2P, and will be built with anonymity in mind. One of the first features of
MCXIRCD was anonymous channels.

MCXIRCD is talking a dialect of [RFC2812](https://www.rfc-editor.org/rfc/rfc2812.txt) that is built from scratch with focus on
both anonymity and hard pseudonymity. This means that a lot of commands will 
never be implemented, and that much of the output generated about other users is
censored.

Features and quirks
-------------------

* You can log into channels and also chat with others (yey!).
* Basic channel functionalities you would expect, such as chanops and topic. No voice.
* There are anonymous and regular channels. Anonymous channels have names that begins with + instead of #.
* Filters and rejects Client To Client-Protocol (CTCP) commands. CTCP can be used to query users about the client software they use, or to trick users into revealing their IP addresses for sending/receiving files.
* Silently translates NOTICE and SQUERY into PRIVMSG. This means that there is no difference between users, bots and services. They are all treated equally - which I think actually makes things easier.
* Accepts at most 20 arguments for all commands. RFC2812 says maximum is 3.

The following list of commands will probably not be implemented: 

* CONNECT
* LINKS
* LUSERS
* TRACE
* SERVLIST
* INVITE

But normal users will almost never, or never-ever use these anyways.

There is a [TODO](TODO.md) file.

Build it
--------
If you are a go developer and have your dev environment setup: `go get butbucket.org/kallevedin/mcxircd/mcxircd`, then build it `go install mcxircd`

If not: I think this is how you should do it on unix-like systems, but you might have to tinker with this yourself.

 * make sure you have the golang compiler installed
 * create a directory structure suitable for go development and set your GOPATH to its root. `cd && mkdir go && cd go; export GOPATH=$(pwd) && mkdir src && mkdir bin`
 * download the code into $GOPATH/src/. `cd src && git clone https://kallevedin@bitbucket.org/kallevedin/mcxircd.git`
 * compile it. `cd $GOPATH && go install mcxircd`
 * run it. `./bin/mcxircd`

Setup
-----
If you want to run MCXIRCD, do this:

* Build it from source
* Edit the mcxircd.conf, MOTD and OPERS files.
* Put all the config files into the same directory, and run `/path/to/where/you/put/mcxircd` in that directory, or just `mcxircd` if the binary is in you $PATH env
* Done! Start your prefered IRC client and connect to mcxircd

Kopimi!
-------
![kompimi](https://bitbucket.org/kallevedin/mcxircd/raw/master/kopimi-unicorn.gif)

Author
------
Kalle Vedin (kalle.vedin [ at ] fripost.org)
