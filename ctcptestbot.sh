#!/bin/sh
# usage:
# ./ctcptestbot.sh | nc 127.0.0.1 6667
#
# it will login with nickname ctcptestb and join channel #hello, where it will
# attempt to dump some CTCP messages.

echo NICK ctcptestb
sleep 1
echo 'JOIN #hello'
echo 'PRIVMSG #hello :CTCP ACTION (should be allowed)'
echo 'PRIVMSG #hello :ACTION IS A MUPPET'
sleep 1
echo 'PRIVMSG #hello :CTCP VERSION (should not be allowed)'
echo 'PRIVMSG #hello :VERSION'
sleep 1
echo 'PRIVMSG #hello :CTCP DERP (should not be allowed)'
echo 'PRIVMSG #hello :DERP'
sleep 3
echo 'PRIVMSG #hello :bai'
sleep 1
